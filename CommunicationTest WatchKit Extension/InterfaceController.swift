//
//  InterfaceController.swift
//  CommunicationTest WatchKit Extension
//
//  Created by Parrot on 2019-10-26.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity
import GameKit
class InterfaceController: WKInterfaceController, WCSessionDelegate {

    
    // MARK: Outlets
    // ---------------------
    @IBOutlet var messageLabel: WKInterfaceLabel!
   var pokName = ""
    var pokType = ""
    @IBOutlet var startButton: WKInterfaceButton!
    // Imageview for the pokemon
    @IBOutlet var pokemonImageView: WKInterfaceImage!
    // Label for Pokemon name (Albert is hungry)
    @IBOutlet var nameLabel: WKInterfaceLabel!
    // Label for other messages (HP:100, Hunger:0)

    
    @IBOutlet var hungerLabel: WKInterfaceLabel!
    @IBOutlet var healthLabel: WKInterfaceLabel!
    @IBOutlet var nameButton: WKInterfaceButton!
    
    // MARK: Delegate functions
    // ---------------------
    var seconds = 0
    var health = 100
    var hunger = 0
    var timer = Timer()
   
    
    // Default function, required by the WCSessionDelegate on the Watch side
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        //@TODO
    }
    
    // 3. Get messages from PHONE
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("WATCH: Got message from Phone")
        // Message from phone comes in this format: ["course":"MADT"]
        let messageBody = message["Pokemon"] as! String
        pokType = messageBody
        messageLabel.setText(messageBody)
        nameButton.setEnabled(true)
        nameButton.setHidden(false)
    }
    


    
    // MARK: WatchKit Interface Controller Functions
    // ----------------------------------
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        
        // 1. Check if teh watch supports sessions
        if WCSession.isSupported() {
            WCSession.default.delegate = self
            WCSession.default.activate()
        }
        
        
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        nameButton.setHidden(true)
        nameButton.setEnabled(false)
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    
    // MARK: Actions
    // ---------------------
    
    // 2. When person presses button on watch, send a message to the phone
    @IBAction func buttonPressed() {
        
        // 1. Show the built in UI for accepting user input
                      let cannedResponses = ["Pok1", "Pok2", "Pok3"]
                      presentTextInputController(withSuggestions: cannedResponses, allowedInputMode: .plain) {
                          
                          (results) in
                          
                          if (results != nil && results!.count > 0) {
                              // 2. write your code to process the person's response
                              let userResponse = results?.first as? String
                              print(userResponse)
                              self.messageLabel.setText(userResponse)
                            self.nameButton.setHidden(true)
                            self.nameLabel.setText("\(userResponse!) is hungry ")
                            self.pokName = userResponse ?? ""
                          }
                      }
                      
        
        
        
    }
    
    
    // MARK: Functions for Pokemon Parenting
    @IBAction func startButtonPressed() {
        print("Start button pressed")
        runTimer()
        
    }
    func runTimer()
    {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(updateTimer)), userInfo: nil, repeats: true)
    }
    @objc
    func updateTimer()
    {
        print("----------------------------")
        print("HUNGER: \(hunger)")
        print("HEALTH: \(health)")
        seconds += 1;
        messageLabel.setText("\(seconds)")
        if seconds % 5 == 0 {
            print("MODE OF 5")
            hunger += 10
            hungerLabel.setText("HUNGER: \(hunger)")
            if hunger > 80 {
                health -= 5
                var updatedHealth = 100 - health
                healthLabel.setText("HP: \(health)")
                if health <= 0 {
                     healthLabel.setText("HP: 0")
                    nameLabel.setText("\(pokName) is dead")
                    timer.invalidate()
                }
            }
           
            
        }
    }
    
    
    
    @IBAction func feedButtonPressed() {
        print("Feed button pressed")
        if hunger >= 12  {
             hunger -= 12
        }
        
       
    }
    
    @IBAction func hibernateButtonPressed() {
        timer.invalidate()
        var T = "t"
        print("Hibernate button pressed")
        if WCSession.default.isReachable {
                   print("Attempting to send message to phone")
                   self.messageLabel.setText("Sending msg to watch")
                   WCSession.default.sendMessage(
                    ["State" : health,"hungerLevel" : hunger,"Name" : pokName, "Type" : pokType, "Wake" : T],
                       replyHandler: {
                           (_ replyMessage: [String: Any]) in
                           // @TODO: Put some stuff in here to handle any responses from the PHONE
                           print("Message sent, put something here if u are expecting a reply from the phone")
                           self.messageLabel.setText("Got reply from phone")
                   }, errorHandler: { (error) in
                       //@TODO: What do if you get an error
                       print("Error while sending message: \(error)")
                       self.messageLabel.setText("Error sending message")
                   })
               }
               else {
                   print("Phone is not reachable")
                   self.messageLabel.setText("Cannot reach phone")
               }
        
    }
    
    
}
