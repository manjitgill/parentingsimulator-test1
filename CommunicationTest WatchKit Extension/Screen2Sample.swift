//
//  Screen2Sample.swift
//  CommunicationTest WatchKit Extension
//
//  Created by Parrot on 2019-10-31.
//  Copyright © 2019 Parrot. All rights reserved.
//

import Foundation
import WatchKit
import WatchConnectivity

@available(watchOSApplicationExtension 6.0, *)
class Screen2Sample: WKInterfaceController, WCSessionDelegate {
    
    // MARK: Outlets
    // ---------------------
    
    // 2. Outlet for the variables
    var flag : Bool = false
    var name : String = ""
    @IBOutlet var startButton: WKInterfaceButton!
   
    // MARK: Delegate functions
    @IBOutlet var nameLabel: WKInterfaceLabel!
    // ---------------------
    
    // Default function, required by the WCSessionDelegate on the Watch side
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        //@TODO
    }
    
    // MARK: WatchKit Interface Controller Functions
    // ----------------------------------
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        
        // 1. Check if the watch supports sessions
        if WCSession.isSupported() {
            WCSession.default.delegate = self
            WCSession.default.activate()
        }
        
        
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        if flag == false {
             super.willActivate()
                   nameLabel.setHidden(true)
                   startButton.setHidden(true)
                   
        }
       
        
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    
    // MARK: Actions
    @IBAction func startButtonPressed() {
        print("Start button pressed")
       
        flag = false
       
    }
    
    
    @IBAction func selectNameButtonPressed() {
        print("select name button pressed")
        // 1. Show the built in UI for accepting user input
               let cannedResponses = ["Pokemonm1", "Pokemon2", "Pokemon3"]
               presentTextInputController(withSuggestions: cannedResponses, allowedInputMode: .plain) {
                   
                   (results) in
                   
                   if (results != nil && results!.count > 0) {
                       // 2. write your code to process the person's response
                       let userResponse = results?.first as? String
                       print(userResponse)
                       self.nameLabel.setText(userResponse)
                    self.name = userResponse ?? ""
                   }
               }
               
        
        
               nameLabel.setHidden(false)
               startButton.setHidden(false)
        flag = true
               
    }
    

}
